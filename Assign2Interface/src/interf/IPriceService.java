package interf;

import java.rmi.Remote;
import java.rmi.RemoteException;

import entities.Car;

public interface IPriceService extends Remote {
	
	double computeSellingPrice(Car c) throws RemoteException;

}

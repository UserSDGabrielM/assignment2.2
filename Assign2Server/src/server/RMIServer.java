package server;

import entities.Constant;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class RMIServer {

	public static void main (String[] args) throws RemoteException, AlreadyBoundException {
		
		TaxImplementation taxImpl = new TaxImplementation();
		PriceImplementation priceImpl = new PriceImplementation();
		
		Registry registry = LocateRegistry.createRegistry(Constant.RMI_PORT);
		registry.bind(Constant.RMI_ID, taxImpl);
		
		Registry registry2 = LocateRegistry.createRegistry(Constant.RMI_PORT2);
		registry2.bind(Constant.RMI_ID_Price, priceImpl);
		
		System.out.println("Server has started.");
		
	}
	
}

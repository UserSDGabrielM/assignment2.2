package client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import entities.Car;
import entities.Constant;
import interf.IPriceService;
import interf.ITaxService;

public class RMIClient {

	private static ClientGUI clientGUI;
	
	public static void main(String[] args) throws RemoteException, NotBoundException {
		// TODO Auto-generated method stub
		Registry registry = LocateRegistry.getRegistry("localhost", Constant.RMI_PORT);
		ITaxService taxServ = (ITaxService) registry.lookup(Constant.RMI_ID);
		
		
		Registry reg2 = LocateRegistry.getRegistry("localhost", Constant.RMI_PORT2);
		IPriceService priceServ = (IPriceService) reg2.lookup(Constant.RMI_ID_Price);
		
		/*Car myCar = new Car(2009,2000);
		myCar.setPurchasePrice(25000);
		System.out.println("The tax value of the car is " + taxServ.computeTax(myCar));
		System.out.println("The selling price of the car is " + priceServ.computeSellingPrice(myCar));
*/
		
		clientGUI = new ClientGUI();
		clientGUI.setVisible(true);
		
		
		class ComputeActionListener implements ActionListener{

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				int productionYear = clientGUI.getYear();
				int engineCapacity = clientGUI.getEngCap();
				double purchasedPrice = clientGUI.getPrice();
			
				if (!("".equals(productionYear) || "".equals(engineCapacity) || "".equals(purchasedPrice))) {
					Car myCar = new Car();
					myCar.setYear(productionYear);
					myCar.setEngineCapacity(engineCapacity);
					myCar.setPurchasePrice(purchasedPrice);
						
					if(productionYear<=0 || engineCapacity<=0 || purchasedPrice<=0)
						
						JOptionPane.showMessageDialog(new JFrame(), "One of the inputs is negative.");
					
					else{
						try {							
							clientGUI.printTax(String.valueOf(taxServ.computeTax(myCar)));
							clientGUI.printPrice(String.valueOf(priceServ.computeSellingPrice(myCar)));						
						} 
						catch (RemoteException e) {						
							e.printStackTrace();
						}					
					}				
				
				}
				
			}
		}
		
		
		clientGUI.addBtnComputeActionListener(new ComputeActionListener());
	
		
	}
}

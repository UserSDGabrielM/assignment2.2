package server;

import entities.Car;
import interf.IPriceService;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class PriceImplementation extends UnicastRemoteObject implements IPriceService {

/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

protected PriceImplementation() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}

@Override
public double computeSellingPrice(Car c){
		
		double sellPrice=0;
		sellPrice = c.getPurchasePrice() - (c.getPurchasePrice()/7) * (2015 - c.getYear());
		return sellPrice;
	}

}

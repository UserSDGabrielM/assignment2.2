package client;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import entities.Car;

public class ClientGUI extends JFrame {
	
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textYear;
	private JTextField textEngCap;
	private JTextField textPrice;
	private JButton btnCompute;
	private JTextArea textAreaTax;
	private JTextArea textAreaPrice;
	
	public ClientGUI() {
		
		setTitle("RMI Car Application");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setPreferredSize(new Dimension(300,400));
		pack();
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(10, 10, 10, 10));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		getContentPane().setBackground(Color.cyan);
		
		JLabel lblYear = new JLabel("Fabrication year");
		lblYear.setBounds(10 ,36 ,120 ,14);
		contentPane.add(lblYear);
		
		JLabel lblEngCap = new JLabel("Engine size");
		lblEngCap.setBounds(10,61,120,14);
		contentPane.add(lblEngCap);
		
		JLabel lblPrice = new JLabel("Purchasing price");
		lblPrice.setBounds(10, 86, 200, 14);
		contentPane.add(lblPrice);
		
		JLabel lblCompTax = new JLabel("The car tax is:");
		lblCompTax.setBounds(10, 203, 120,14 );
		contentPane.add(lblCompTax);
		
		JLabel lblCompPrice = new JLabel("The car's price is:");
		lblCompPrice.setBounds(10,253, 120, 14);
		contentPane.add(lblCompPrice);
		
		textYear = new JTextField();
		textYear.setBounds(120, 33, 86, 20);
		contentPane.add(textYear);
		textYear.setColumns(10);

		textEngCap = new JTextField();
		textEngCap.setBounds(120, 58, 86, 20);
		contentPane.add(textEngCap);
		textEngCap.setColumns(10);

		textPrice = new JTextField();
		textPrice.setBounds(120, 83, 86, 20);
		contentPane.add(textPrice);
		textPrice.setColumns(10);
		
		btnCompute = new JButton("Compute");
		btnCompute.setBounds(10, 132, 89, 23);
		contentPane.add(btnCompute);
		
		textAreaTax = new JTextArea();
		textAreaTax.setBounds(120, 200, 90, 20);
		contentPane.add(textAreaTax);
		
		textAreaPrice = new JTextArea();
		textAreaPrice.setBounds(120, 250, 90, 20);
		contentPane.add(textAreaPrice);
		
	}
	
	public void addBtnComputeActionListener(ActionListener e) {
		btnCompute.addActionListener(e);
	}

	public int getYear() {
		return Integer.parseInt(textYear.getText());
	}

	public int getEngCap() {
		return Integer.parseInt(textEngCap.getText());
	}

	public double getPrice() {
		return Integer.parseInt(textPrice.getText());
	}
	
	public void printTax(String tax){
		textAreaTax.setText(tax);
	}
	
	public void printPrice(String price){
		textAreaPrice.setText(price);
	}
	
	public void clear(){
		textYear.setText("");
		textEngCap.setText("");
		textPrice.setText("");
	}
}
